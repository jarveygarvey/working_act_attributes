class ActivitiesController < ApplicationController
    before_action :set_user

    
    def index
        @activities = Activity.all
        
        @activity_type = params[:activity_type]
        
        if @activity_type == "1"
            $activity_type_var = 1
        elsif @activity_type == "2"
            $activity_type_var = 2
        else
            $activity_type_var = 3
        end
        

        #@week = Week.find(params[:activities_id])
        #@week_id = @week.id-1
    end
    
    def new
        @activity = Activity.new
    end
    
    
    def create
        @activity = Activity.new(activity_params)
        if @activity.save
            #UserMailer.activity_added(@user,@activity).deliver
            @chosen_award_user.times do |index|
              Week.create(activities_id: @activity.id, week_num: index+1, date_completed: 2016-01-01, submitted: false, approved: false)
        end 
            
            
            if $activity_type_var == 1
                $activity_type_var = 2
                redirect_to new_user_activity_path(@user)
                
            elsif $activity_type_var == 2
                $activity_type_var = 3
                redirect_to new_user_activity_path(@user)
            else
                redirect_to home_path(@user)
            end
        else
            redirect_to root_path(@user)
        end
    end
    
    private
    
    def activity_params
        params.require(:activity).permit(:users_id, :activity_type, :activity_name, :approver_email)
    end
    
    def set_user
        @user = User.find(params[:user_id])
        @chosen_award_user = @user.chosen_award
    end
    

end