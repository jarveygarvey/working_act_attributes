class WeeksController < ApplicationController
    before_action :set_activity
    before_action :set_user
    before_action :set_week_num

    def index
        #@week_number = params[:week_num]
        @weeks = Week.all
        
        #just finding the activity id of the week
        @activity = Activity.find(params[:activity_id])
        @activity_id = params[:activity_id]
        
        #Gets the activity attributes for the week that you've selected.
        @activity_type = @activity.activity_type
        @activity_name = @activity.activity_name
        
        #Find current week object
        @week = Week.where(activities_id:@activity_id, week_num:@week_number)
        
        #For posting the status of the week's submission/approval
        @weeks.where(activities_id:@activity_id, week_num:@week_number).each do |week|
            if week.submitted==false
                @submitted = 'Not submitted'
            else
                @submitted = 'Submitted'
            end
            if week.approved==false
                @approved = 'Not approved'
            else
                @approved = 'Approved'
            end
        end 

        
    end
    
    def edit
        #@week_number = params[:week_num]
        @weeks = Week.all
        
        #just finding the activity id of the week
        @activity = Activity.find(params[:activity_id])
        @activity_id = params[:activity_id]
        @user = User.find(params[:user_id])
        
        #Gets the activity attributes for the week that you've selected.
        @activity_type = @activity.activity_type
        @activity_name = @activity.activity_name
        
        #Find current week object
        @week = Week.find(params[:id])
        
    end
    
    def update
        @week = Week.find(params[:id])
        @week_id = params[:id]
        @weeks = Week.all
        
        @weeks.where(id: @week_id).each do |week|
            @week_num = week.week_num
        end 
        
        if @week.update_attributes(week_params)
            UserMailer.update_week(@user,@activity,@week).deliver
            redirect_to user_activity_weeks_path(:user_id =>  @user_id, :activity_id => @activity_id, :activity_type => $activity_type_var, :week_num => @week_num)
        else
            render root_path
        end
    end
    
    
    def new
        #@week_number = params[:week_num]
        @week= Week.new
    end
    
    def create
        @week = Week.new(week_params)
        if @week.save
            redirect_to home_path
        else
            render root_path
        end
    end
    
    
    
    private
    
    def week_params
        params.require(:week).permit(:activities_id, :week_num, :date_completed, :submitted, :approved)
    end
    
    def set_activity
        #just finding the activity id of the week
        @activity = Activity.find(params[:activity_id])
        @activity_id = params[:activity_id]
    end
    
    def set_user
        @user = current_user
        @user_id = params[:user_id]
    end
    
    def set_week_num
        @week_number = params[:week_num]
    end
    

end