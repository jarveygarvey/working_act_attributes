class UsersController < ApplicationController
    skip_before_action :require_login, only: [:new, :create, :landing]
    def landing
    
    end
    
    def new
      @user = User.new
    end
    
    def create
        @user = User.new(user_params)
        $activity_type_var = 1
      if @user.save
          login(params[:user][:email], params[:user][:password])
          flash[:success] = 'Welcome!'
          #want to send an email to users on registration, giving them instructions for how they login in the future, and a guide on how to             use the app effectively
          #UserMailer.signup_confirmation(@user).deliver
          redirect_to new_user_activity_path(@user)
      else
          render root_path
      end
    end

  private

  def user_params
      params.require(:user).permit(:first_name, :surname, :previous_award, :chosen_award, :email, :password, :password_confirmation, :name)
  end
end