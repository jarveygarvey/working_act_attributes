class User < ActiveRecord::Base
    has_many :activities, dependent: :destroy
    has_many :weeks, dependent: :destroy
    authenticates_with_sorcery!
    validates :password, length: { minimum: 3 }
    validates :password, confirmation: true
    validates :email, uniqueness: true, email_format: { message: 'has invalid format' }

    def setPersonal
        $activity_type_var = 1
        return "Personal Activity"
    end
    
    def setPhysical
        $activity_type_var = 2
        return "Physical Activity"
    end
    
    def setCommunity
        $activity_type_var = 3
        return "Community Activity"
    end
end