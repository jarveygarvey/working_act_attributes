class Activity < ActiveRecord::Base
    belongs_to :user
    has_many :weeks, dependent: :destroy
    
    $activity_type_var = 1;

end