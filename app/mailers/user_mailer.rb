class UserMailer < ApplicationMailer

    def signup_confirmation(users)
        @user = users
        mail to: users.email, from: "jessica92garvey@gmail.com", subject: "Signup Confirmation"
    end
    
    def activity_added(users,activities)
        @user = users
        @activity = activities
        mail to: activities.approver_email, from: users.email, subject: "MyGaisce: Activity activation"
    end
    
    def update_week(users,activities,weeks)
        @user = users
        @activity = activities
        @week = weeks
        mail to: activities.approver_email, from: users.email, subject: "MyGaisce: Please approve my activity"
    end
end
