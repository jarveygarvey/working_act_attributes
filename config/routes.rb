Rails.application.routes.draw do
    
    get '/sup_home', to: 'supervisors#home', as: :sup_home
    get '/facebook', to: 'social#facebook', as: :facebook
    get '/twitter', to: 'social#twitter', as: :twitter
    get '/website', to: 'social#website', as: :website
    get '/instagram', to: 'social#instagram', as: :instagram
    
    get '/secret', to: 'pages#index', as: :secret
    
    resources :users, only: [:new, :create] do
        resources :activities do
            resources :weeks
        end
    end
        
    get '/sign_up', to: 'users#new', as: :sign_up
    get '/home', to: 'users#home', as: :home
    get '/landing', to: 'users#landing', as: :landing
    root to: 'users#landing'
    
    resources :sessions, only: [:new, :create, :destroy]
    get '/log_in', to: 'sessions#new', as: :log_in
    delete '/log_out', to: 'sessions#destroy', as: :log_out
end
