# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160205191420) do

  create_table "activities", force: :cascade do |t|
    t.integer  "activity_type"
    t.string   "activity_name"
    t.string   "approver_email"
    t.integer  "users_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "activities", ["users_id"], name: "index_activities_on_users_id"

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "surname"
    t.integer  "previous_award"
    t.integer  "chosen_award"
    t.string   "email",                        null: false
    t.string   "crypted_password"
    t.string   "salt"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remember_me_token"
    t.datetime "remember_me_token_expires_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["remember_me_token"], name: "index_users_on_remember_me_token"

  create_table "weeks", force: :cascade do |t|
    t.integer  "week_num"
    t.date     "date_completed"
    t.boolean  "submitted"
    t.boolean  "approved"
    t.integer  "activities_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "weeks", ["activities_id"], name: "index_weeks_on_activities_id"

end
