class RemoveTimeFromWeeks < ActiveRecord::Migration
  def change
    remove_column :weeks, :time_completed, :time
  end
end
