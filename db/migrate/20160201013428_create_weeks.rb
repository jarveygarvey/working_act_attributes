class CreateWeeks < ActiveRecord::Migration
  def change
    create_table :weeks do |t|
      t.integer :week_num
      t.date :date_completed
      t.time :time_completed
      t.boolean :submitted
      t.boolean :approved
      t.references :activities, index: true, foreign_key: true
      t.references :users, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
