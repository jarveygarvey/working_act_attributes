class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.integer :activity_type
      t.string :activity_name
      t.string :approver_email
      t.references :users, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
